var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');

//services
var sDevices = require('./services/devices');
var sUsers = require('./services/users');

//app
var app = express();

// websocket
sDevices.include(app);
sUsers.include(app);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

/**
 * database
 */
// modelling loading
var mongoose = require ("mongoose");
mongoose.Promise = require('bluebird');
var dbName = process.env.db || 'SmartHome';
var dbHost = process.env.DB_HOST || 'localhost';
var dbPort = process.env.DB_PORT || 27017;
var uristring = "mongodb://"+dbHost+"/"+dbName;
mongoose.connect(uristring, {useMongoClient: true}, function (err, res) {
      if (err) {
          console.log ('ERROR connecting to: ' + uristring + '. ' + err);
      } else {
          console.log ('Succeeded connected to: ' + uristring);
      }
});

module.exports = app;

