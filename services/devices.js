/**
 * Devices module.
 * @module services/devices
 * 
 * Connect to this server:
 *  wscat -n -c wss://localhost:3001/devices
 */

var express = require('express');
var https = require('https');
var fs = require('fs');
var WebSocket = require('ws');
var mongoose = require ("mongoose");

var dev_model = require('../models/device_model.js');
var master = require('./master.js');

const ops = 'ops';
const ops_t = 'string';
const ops_d = ['sign in', 'set light', 'light set', 'detected person'];
const DEVs = 'DEVs';
const DEVs_a_t = 'object';

/**
 * Check if id exist in data base
 * @param {string} ID - id of the device
 * @param {object} devmodel - device model (database)
 * @param {object} callback - call back function
 * @return {object} callback of the execution
 */
function checkID(ID, devmodel, callback){
    var query = devmodel.find({'id':ID.dev[0],'pass':ID.dev[1]});

    query.exec(function (err, result) {
        if (result.length === 0) {
            callback(false, null);
        }
        else {
            return callback(true, result[0]);
        }
    });
}

/**
 * Check if data is ok
 * @param {string} data - data send from device that need to be checked
 * @return {boolean} true if data is ok
 */
function checkValidData(data) {
    // check ops
    if(!data.hasOwnProperty(ops) || !(typeof(data.ops) === ops_t)) {
        return false;
    }
    if(ops_d.indexOf(data.ops) === -1) {
        return false;
    }

    // check DEVs
    if (!data.hasOwnProperty(DEVs) || !Array.isArray(data.DEVs) || data.DEVs.length == 0) {
        return false;
    }
    for(var i = 0; i < data.DEVs.length; i++) {
        if(typeof(data.DEVs[i]) !== DEVs_a_t) {
            return false;
        }
    }

    return true;
}

/**
 * Check if device is login and the id of device is valid for operation (ws connection)
 * @param {object} - ws websocket connection
 * @param {string} - id id for operation
 * @return {boolean} true if valid, also send error message and close connection
 */
function checkValidOperation(ws, id) {
    // check login status
    if(ws.privatedata === null) {
        console.log('not login yet');
        ws.send('{"reply":"_notgood","problems":[2583]}', function(error) {});
        ws.close();
        return false;
    }

    // check if id is right
    if(id !== ws.privatedata.id) {
        console.log('wrong id');
        ws.send('{"reply":"_notgood","problems":[7604]}', function(error) {});
        ws.close();
        return false;
    }

    return true;
}

// list of devices
var devices = [];

/** Create a websocket server for handling connection from devices */
exports.include = function(app) {

    // keys for wss
    var privateKey  = fs.readFileSync('private/key/wss/private.key', 'utf8');
    var certificate = fs.readFileSync('private/key/wss/certificate.pem', 'utf8');
    var credentials = {key: privateKey, cert: certificate};

    // create https server
    app.wssDeviceServer = https.createServer(credentials);

    // wss server
    var wss = new WebSocket.Server({
        server: app.wssDeviceServer,
        path: '/devices'
    });

    // get the database
    var database = mongoose.model(dev_model.modeltable, dev_model.schema);

    // handle devices
    wss.on('connection', function connection(ws, req) {

        // data of device
        ws.privatedata = null;
        // connection status
        ws.isAlive = true;

        /**
         * Handle events from users
         * @param {string} data - data from request
         */
        var userRequestHandler = function(data) {
            console.log(data);

            // check if connection is valid
            if(ws.privatedata === null) {
                console.log('not login yet');
                ws.send('{"reply":"_notgood","problems":[2583]}', function(error) {});
                ws.close();
                return;
            }

            // handle events
            switch(data.action) {
            case 'set light':
                // responsible for set light action from user
                // TODO  put ws.send() out of this function?
                // {"ops":"set light","DEVs":[{"dev":["00:00:00:00:00:00_1","0000"]}]}
                var devMess = {ops: 'set light', DEVs: [{dev: [ws.privatedata.id, data.value]}]};
                ws.send(JSON.stringify(devMess), function(error) {
                    if(error == null) {
                        database.findOneAndUpdate({id: ws.privatedata.id, pass : ws.privatedata.pass}, {$set:{light : data.value}}, function(err, doc) {
                            // database is updated successfully
                            if(err === null) {
                                ws.privatedata.light = data.value;
                            }
                        });
                    }
                });
                console.log('DEVICES: event done');

                break;
            case 'ask light':
                // responsible for ask light action from user
                console.log('DEVICES: event done');
                var toUser = {action: 'dev_report: ask light', DEVs: [{dev: [ws.privatedata.id, ws.privatedata.light]}]};
                master.emit(data.userId, toUser);

                break;
            }
        };

        ws.on('message', function incoming(message) {
            console.log('\nreceived: %s', message);

            // json data of message
            var data;

            // prase json from received message
            try {
                data = JSON.parse(message);
            } catch (ex) {
                console.log('data is not json');
                console.log(ex);
                ws.send('{"reply":"_notgood","problems":[3396]}', function(error) {});
                ws.close();
                return;
            }

            // check if data is good
            if(checkValidData(data) === false) {
                console.log('data is not valid');
                ws.send('{"reply":"_notgood","problems":[3396]}', function(error) {});
                ws.close();
                return;
            }

            // check valid operation (check if device is ok)
            if(data.ops !== 'sign in') {
                if(checkValidOperation(ws, data.DEVs[0].dev[0]) === false) {
                    return;
                }
            }

            // ops
            switch(data.ops) {
            case 'sign in':
                // Sign in
                console.log('sign in');

                checkID(data.DEVs[0], database, function(err, result) {
                    if(err === true) {
                        // update device's status
                        database.findOneAndUpdate({id : result.id, pass : result.pass}, {$set:{online : true}}, function(err, doc) {
                            if(err === null){
                                result.online = true;

                                ws.privatedata = result;
                                devices.push(ws);

                                console.log('ok');
                                ws.send('{"reply":"_ok"}', function(error) {});
                                // start listening command from user 
                                master.on(ws.privatedata.id, userRequestHandler);

                                return;
                            } else {
                                console.log('database problem');
                                ws.send('{"reply":"_notgood","problems":[4064]}', function(error) {});
                                return;
                            }
                        });
                    } else {
                        console.log('id and pass problem');
                        ws.send('{"reply":"_notgood","problems":[5987]}', function(error) {});
                        ws.close();
                        return;
                    }
                });

                break;
            case 'light set':
                // Device set light to server
                console.log('light set');

                if(ws.privatedata.online === true) {
                    database.findOneAndUpdate({id:ws.privatedata.id, pass : ws.privatedata.pass}, {$set:{light : data.DEVs[0].dev[1]}}, function(err, doc) {
                        if(err === null) {
                            console.log('ok');
                            ws.send('{"reply":"_ok"}', function(error) {});
                            return;
                        } else {
                            console.log('database problem');
                            ws.send('{"reply":"_notgood","problems":[4064]}', function(error) {});
                            return;
                        }
                    });
                }

                break;
            case 'detected person':
                // Device detected person
                console.log('detected person');

                console.log('not supported yet');
                ws.send('{"reply":"_notgood","problems":[1288]}', function(error) {});
                ws.close();

                break;
            default:
                console.log('default');

                ws.send('{"reply":"_notgood","problems":[1288]}', function(error) {});
                ws.close();

                break;
            }
        });

        /**
         * @brief   listen to pong from client
         */
        ws.on('pong', function() {
            ws.isAlive = true;
        });

        // handle close
        ws.on('close', function() {
            console.log('DEVICES: websocket closed');

            if(ws.privatedata !== null) {
                database.findOneAndUpdate({id:ws.privatedata.id, pass : ws.privatedata.pass}, {$set:{online : false}}, function(err, doc) {});
                // remove listener of this device
                master.removeListener(ws.privatedata.id, userRequestHandler);

                delete ws.privatedata;
                ws.privatedata = null;
            }

            ws.terminate();
        });

        // handle error
        ws.on("error", function(error) {
            // Manage error here
            console.log(error);
        });
    });

    // check for connection status every 5 second
    const interval = setInterval(function ping() {      
        var i = 0;
        for(i = 0; i < wss.clients.length; i++) {
            if (wss.clients[i].isAlive === false) {
                console.log('DEVICES: Ping: websocket closed');
                //close connection protocol
                wss.clients[i].close();
                //go to close connection protocol
                wss.clients[i].emit('close');
                wss.clients.splice(i, 1);
                return;
            }

            wss.clients[i].isAlive = false;
            wss.clients[i].ping('', false, true);
        }

    }, 5000);

};

