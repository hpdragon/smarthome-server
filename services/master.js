/**
 * Master module.
 * @module services/master
 */

var events = require('events');

var eventEmitter = new events.EventEmitter();

/** Event emitter of master devices */
module.exports = eventEmitter;

