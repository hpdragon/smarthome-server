/**
 * Users module.
 * @module services/users
 * 
 * Connect to this server:
 *  wscat -n -c wss://localhost:3002/users
 */

var express = require('express');
var https = require('https');
var fs = require('fs');
var WebSocket = require('ws');
var mongoose = require ("mongoose");
var diff = require('deep-diff').diff;

var usr_model = require('../models/user_model.js');
var master = require('./master.js');

const _ops = 'ops';
const _ops_t = 'string';
const _ops_d = ['sign up', 'sign in', 'sign out', 'set light', 'ask light'];
const _DEVs = 'DEVs';
const _DEVs_a_t = 'object';

// format of requests
const _format_signup = {"ops":"sign up","usr":[{"email":"email"},{"password":"password"},{"name":"name"}]};
const _format_signin = {"ops":"sign in","usr":[{"email":"email"},{"password":"password"}]};
const _format_signout = {"ops":"sign out"};
const _format_setlight = {"ops":"set light","DEVs":[{"dev":["dev_name","stt"]},{"dev_name":["dev_name","stt"]}]};
const _format_asklight = {"ops":"ask light","DEVs":[{"dev":["dev_name"]},{"dev":["dev_name"]}]};

/**
 * Compare two json variable
 * @param {object} scr - origin object to compare
 * @param {object} des - need to compare object
 * @return {boolean}
 *  - true: everything is similar except element (element may like or difference)
 *  - false: have some thing difference
 */
function _compareObject(scr, des) {
    var differences = diff(scr, des);
    
    if(differences == null) {
        return true;
    } else{
        for(var i = 0; i < differences.length; i++) {
            if(differences[0].kind !== 'E') {
                return false;
            }
        }
    }
}

/**
 * Check if data format is ok
 * @param {string} data - data to check (messege from users)
 * @return {boolean}
 *  - true:     data is ok
 *  - false:    data is not ok
 */
function checkValidData(data) {
    // check ops
    if(!data.hasOwnProperty(_ops) || !(typeof(data.ops) === _ops_t)) {
        return false;
    }
    if(_ops_d.indexOf(data.ops) === -1) {
        return false;
    }

    // check data structure
    switch(data.ops) {
    case 'sign up':
        return _compareObject(_format_signup, data);

        break;
    case 'sign in':
        return _compareObject(_format_signin, data);

        break;
    case 'sign out':
        return _compareObject(_format_signout, data);

        break;
    case 'set light':
        return _compareObject(_format_setlight, data);

        break;
    case 'ask light':
        return _compareObject(_format_asklight, data);

        break;
    default:
        return false;

        break;
    }

    return true;
}

/**
 * Sign up new user
 * @param {string} user - id of the user ({"email":"<email of user>", "password": "<password of user>","name":"<name of user>"})
 * @param {object} callback - callback if finished
 * @return {object}
 *  - 0: account created
 *  - 1: cant save to database
 *  - 2: already have account
 */
var signUpID_User = function (user, usrmodel, callback) {
    var userQ = usrmodel.find({'email': user.email});

    userQ.exec(function (err, result) {
        if (result.length == 0) {
            // create new user
            var newUser = {email: user.email, password: user.password, detail: {name: user.name, date_of_birth: null, gender: null}, own_devices: [], online: true};

            usrmodel.create(newUser, function (err, newUser) {
                    if (err){
                        return callback(1);
                }
                // saved!
                //console.log('saved');
                return callback(0);
            });
        }
        else return callback(2);
    });
};

/**
 * Check if id exist and have ok password in data base
 * @param {string} ID - id of the user ({"email":"<email of user>", "password": "<password of user>"})
 * @param {object} usrmodel - model of user from database (model of mongoose)
 * @param {object} callback - callback if finished
 * @return {object}
 *  - callback(false,null):         id not exist or wrong passwrod
 *  - callback(true, result[0]):    ok for sign in
 */
function signInID(ID, usrmodel, callback){
    var query = usrmodel.find({'email': ID.email,'password': ID.password});

    query.exec(function (err, result) {
        if (result.length === 0) {
            callback(false, null);
        }
        else {
            return callback(true, result[0]);
        }
    });
}

/**
 * Check if user is login and the id of user is valid for operation (ws connection)
 * @param {object} ws - websocket connection
 * @return {boolean} true if valid, also send error message and close connection
 */
function checkValidOperation(ws) {
    // check login status
    if(ws.privatedata === null) {
        console.log('not login yet');
        ws.send('{"reply":"_notgood","problems":[3237]}', function(error) {});
        ws.close();
        return false;
    }

    return true;
}

/** Create a websocket server for handling connection from users */
exports.include = function(app) {

    // keys for wss
    var privateKey  = fs.readFileSync('private/key/wss/private.key', 'utf8');
    var certificate = fs.readFileSync('private/key/wss/certificate.pem', 'utf8');
    var credentials = {key: privateKey, cert: certificate};

    // create https server
    app.wssUserServer = https.createServer(credentials);

    // wss server
    var wss = new WebSocket.Server({
        server: app.wssUserServer,
        path: '/users'
    });

    // get the database
    var database = mongoose.model(usr_model.modeltable, usr_model.schema);

    // handle devices
    wss.on('connection', function connection(ws, req) {

        // data of user
        ws.privatedata = null;
        // connection status
        ws.isAlive = true;

        /**
         * Handle events from devices
         * @param {string} data - data from request
         */
        var deviceRequestHandler = function(data) {
            console.log(data);

            //check login status
            if(ws.privatedata === null) {
                console.log('not login yet');
                ws.send('{"reply":"_notgood","problems":[3237]}', function(error) {});
                ws.close();
                return;
            }

            //handle events
            switch(data.action) {
            case 'dev_report: ask light':
                //responsible for the report of device on ask light action from user
                //TODO  put ws.send() out of this function?
                var own_devices = ws.privatedata.own_devices;
                for(var i = 0; i < own_devices.length; i++) {
                    if(own_devices[i].dev_id === data.DEVs[0].dev[0]) {
                        var userMess = {reply: "_ok", DEVs: [{dev: [own_devices[i].dev_name, data.DEVs[0].dev[1]]}]};
                        ws.send(JSON.stringify(userMess), function(error) {});
                    }
                }

                break;
            }

        }

        // handle messages
        ws.on('message', function incoming(message) {
            console.log('\nreceived: %s', message);

            // json data of message
            var data;

            // prase json from received message
            try {
                data = JSON.parse(message);
            } catch (ex) {
                console.log('data is not json');
                console.log(ex);
                ws.send('{"reply":"_notgood","problems":[2449]}', function(error) {});
                ws.close();
                return;
            }

            // check if data is good
            if(checkValidData(data) === false) {
                console.log('data is not valid');
                ws.send('{"reply":"_notgood","problems":[2449]}', function(error) {});
                ws.close();
                return;
            }

            // check valid operation (check if device is ok)
            if(data.ops !== 'sign in' && data.ops !== 'sign up') {
                if(checkValidOperation(ws) === false) {
                    return;
                }
            }

            // ops
            switch(data.ops) {
            case 'sign up':
                console.log('sign up');

                // get user data
                var ID = {email:{type:String}, password:{type:String}, name:{type:String}};
                for(var i = 0; i < data.usr.length; i++) {
                    if(typeof(data.usr[i].email) === 'string') {
                        ID.email = data.usr[i].email;
                    }
                    if(typeof(data.usr[i].password) === 'string') {
                        ID.password = data.usr[i].password;
                    }
                    if(typeof(data.usr[i].name) === 'string') {
                        ID.name = data.usr[i].name;
                    }
                }

                // sign up
                signUpID_User(ID, database, function(result){
                    switch(result){
                    case 0:
                        // account created
                        ws.send('{"reply":"_ok"}', function(error) {});
                        ws.close();

                        break;
                    case 1:
                        // cant save to database

                        break;
                    case 2:
                        // already have account
                        ws.send('{"reply":"_notgood","problems":[6206]}', function(error) {});
                        ws.close();

                        break;
                    }
                });             

                break;
            case 'sign in':
                console.log('sign in');

                // get user data
                var ID_forSignIn = {email:{type:String}, password:{type:String}};
                for(var i = 0; i < data.usr.length; i++) {
                    if(typeof(data.usr[i].email) === 'string') {
                        ID_forSignIn.email = data.usr[i].email;
                    }
                    if(typeof(data.usr[i].password) === 'string') {
                        ID_forSignIn.password = data.usr[i].password;
                    }
                }

                // sign in
                signInID(ID_forSignIn, database, function(err, result) {
                    if(err === true) {
                        // ok for sign in
                        database.findOneAndUpdate({email : result.email, password : result.password}, {$set:{online : true}}, function(err, doc) {
                            if(err === null) {
                                doc.online = true;
                                ws.privatedata = doc;

                                console.log('ok');
                                ws.send('{"reply":"_ok"}', function(error) {});
                                // start listening things from devices
                                master.on(ws.privatedata.email, deviceRequestHandler);

                                return;
                            }
                        });
                        return;
                    } else {
                        // id not exist or wrong password
                        console.log('id and pass problem');
                        ws.send('{"reply":"_notgood","problems":[1618]}', function(error) {});
                        ws.close();
                        return;
                    }
                });

                break;
            case 'sign out':
                console.log('sign out');

                // TODO  need to destroy things here
                ws.send('{"reply":"_ok"}', function(error) {});
                ws.close();
                return;

                break;
            case 'set light':
                console.log('set light');

                // get devices ids
                // TODO  make this algorithm not confusing between don't have devices and not online devices
                var own_devices = ws.privatedata.own_devices;
                var findDev = 0, notFindDev = 0, notOnlineDev = 0;
                var i = 0, j = 0;
                for(i = 0; i < data.DEVs.length; i++) {
                    for(j = 0; j < own_devices.length; j++) {
                        // check if device have in database
                        if(data.DEVs[i].dev[0] === own_devices[j].dev_name) {
                            // send action to devices
                            var trasmit = {action:'set light', value: data.DEVs[i].dev[1]};
                            if(master.emit(own_devices[j].dev_id, trasmit) === true) {
                                console.log('USERS: send event');
                            } else {
                                console.log('USERS: device is not online');
                                notOnlineDev++;
                            }

                            findDev++;
                        }
                    }
                }

                // check if don't have/not online/... devices
                // TODO  return error need to be more specific
                if(findDev === data.DEVs.length && notOnlineDev === 0) {
                    ws.send('{"reply":"_ok"}', function(error) {});
                } else {
                    // don't have devices or this device is not online
                    console.log('devices is not online');
                    ws.send('{"reply":"_notgood","problems":[2487]}', function(error) {});
                }

                break;
            case 'ask light':
                console.log('ask light');

                // get device ids
                // TODO  make this algorithm not confusing between don't have devices and not online devices
                var own_devices = ws.privatedata.own_devices;
                var findDev = 0, notFindDev = 0, notOnlineDev = 0;
                var i = 0, j = 0;
                for(i = 0; i < data.DEVs.length; i++) {
                    for(j = 0; j < own_devices.length; j++) {
                        // check if device have in database
                        if(data.DEVs[i].dev[0] === own_devices[j].dev_name) {
                            // send action to devices
                            var trasmit = {action: 'ask light', userId: ws.privatedata.email};
                            if(master.emit(own_devices[j].dev_id, trasmit) === true) {
                                console.log('USERS: send event');
                            } else {
                                console.log('USERS: device is not online');
                                notOnlineDev++;
                            }

                            findDev++;
                        }
                    }
                }

                // check if don't have/not online/... devices
                // TODO  return error need to be more specific
                if(findDev === data.DEVs.length && notOnlineDev === 0) {
                    // TODO  should reply ok & tell light message or just tell light message???
                    // just reply tell light message, for now
                    //ws.send('{"reply":"_ok"}', function(error) {});
                } else {
                    // don't have devices or this device is not online
                    console.log('devices is not online');
                    ws.send('{"reply":"_notgood","problems":[2487]}', function(error) {});
                }

                break;
            default:
                console.log('default');
                ws.send('{"reply":"_notgood","problems":[4333]}', function(error) {});
                ws.close();
        
            break;
            }
        });

        // @brief   listen to pong from client
        ws.on('pong', function() {
            ws.isAlive = true;
        });

        // handle close
        ws.on('close', function() {
            console.log('USERS: websocket closed');

            if(ws.privatedata !== null) {
                database.findOneAndUpdate({email:ws.privatedata.email, password : ws.privatedata.password}, {$set:{online : false}}, function(err, doc) {});
                // remove listener of this user
                master.removeListener(ws.privatedata.email, deviceRequestHandler);

                delete ws.privatedata;
                ws.privatedata = null;
            }

            ws.terminate();
        });

        // handle error
        ws.on("error", function(error) {
            // manage error here
            console.log(error);
        });

    });

    // check for connection status every 5 second
    const interval = setInterval(function ping() {
        var i = 0;
        for(i = 0; i < wss.clients.length; i++) {
            if (wss.clients[i].isAlive === false) {
                console.log('USERS: Ping: websocket closed');
                // close connection protocol
                wss.clients[i].close();
                // go to close connection protocol
                wss.clients[i].emit('close');
                wss.clients.splice(i, 1);
                return;
            }

            wss.clients[i].isAlive = false;
            wss.clients[i].ping('', false, true);
        }

    }, 5000);

};

