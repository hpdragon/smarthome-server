/**
 * User model module.
 * @module models/user_model
 */

var mongoose = require('mongoose');

/** Model table of users */
exports.modeltable = "users";

/** Schema of users */
exports.schema = new mongoose.Schema({
    email:      {type : String},
    password:   {type : String},
    detail: {
        name:           {type : String},
        date_of_birth:  {type : String},
        gender:         {type : String}
    },
    own_devices : [{
        dev_name:   {type : String},
        dev_id:     {type : String},
        dev_type:   {type : String}
    }],
    online:     {type : Boolean}
});

