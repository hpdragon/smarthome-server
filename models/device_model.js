/**
 * Device model module.
 * @module models/device_model
 */

var mongoose = require ("mongoose");

/** Model table of devices */
exports.modeltable = "devices";

/** Schema of devices */
exports.schema = new mongoose.Schema({
    id : String,
    pass : String,
    light : String,
    online : false,
});

