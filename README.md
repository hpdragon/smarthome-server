# SmartHome-Server

This is a smart home server project for smart home system, the server will handle smart devices and users operation.

## Usage

### Devices

- Connect to devices server:

    ```
    wscat -n -c wss://localhost:3001/devices
    ```

- Standard message from devices (_Station Mode_):

  - Sign in

    ```
    {"ops":"sign in","DEVs":[{"dev":["id","pass"]},{"dev":["id", "pass"]}]}
    ```
    ```
    {"ops":"sign in","DEVs":[{"dev":["00:00:00:00:00:00_1","device"]}]}
    ```

  - Device set light to server

    ```
    {"ops":"light set","DEVs":[{"dev":["id","stt"]},{"dev":["id","stt"]}]}
    ```
    ```
    {"ops":"light set","DEVs":[{"dev":["00:00:00:00:00:00_1","0000"]}]}
    ```

  - Device detected person

    ```
    {"ops":"detected person","DEVs":[{"dev":["id","stt"]}]}
    ```

- Standard message to devices (_Station Mode_):

  - Server set light to device

    ```
    {"ops":"set light","DEVs":[{"dev":["id","stt"]},{"dev":["id","stt"]}]}
    ```
    ```
    {"ops":"set light","DEVs":[{"dev":["00:00:00:00:00:00_1","0000"]}]}
    ```

- Standard message from users (_Access Point Mode_):

- Standard message to users (_Access Point Mode_):

- Standard error return to devices from server:

  - Standard ok reply:

    ```
    {"reply":"_ok"}
    ```
    
  - Standard not ok reply:

    ```
    {"reply":"_notgood","problems":[<problem id1>,<problem id2>]}
    ```

    `<problem id#>`: is problem id, has value in 1000-9999

      - `3396`: bad data type
      - `5987`: don't know this id or wrong password
      - `4064`: database problem
      - `2583`: has not login
      - `1288`: unsupported service
      - `7604`: use id difference than the id that was login
    

### Users:


- Connect to users server:

    ```
    wscat -n -c wss://localhost:3002/users
    ```

- Standard message from users:

  - Sign up:

    ```
    {"ops":"sign up","usr":[{"email":"email"},{"password":"password"},{"name":"name"}]}
    ```
    
  - Sign in:

    ```
    {"ops":"sign in","usr":[{"email":"email"},{"password":"password"}]}
    ```

  - Sign out

    ```
    {"ops":"sign out"}
    ```

  - Set light status to devices

    ```
    {"ops":"set light","DEVs":[{"dev":["dev_name","stt"]},{"dev_name":["dev_name","stt"]}]}
    ```
    ```
    {"ops":"set light","DEVs":[{"dev":["den 1","5000000000000000"]}]}
    {"ops":"set light","DEVs":[{"dev":["den 1","5255255255255255"]}]}
    ```

  - Ask light status from devices

    ```
    {"ops":"ask light","DEVs":[{"dev":["dev_name"]},{"dev":["dev_name"]}]}
    ```
    ```
    {"ops":"ask light","DEVs":[{"dev":["den 1"]}]}
    ```


- Standard message to devices:

  - Tell light status to users

    ```
    {"ops":"tell light","DEVs":[{"dev":["dev_name","stt"]},{"dev":["dev_name","stt"]}]}
    ```
    ```
    {"ops":"tell light","DEVs":[{"dev":["den 1","5000000000000000"]}]}
    ```

- Standard error return to devices from server:

  - Standard ok reply:

    ```
    {"reply":"_ok"}
    ```
    
  - Standard not ok reply:

    ```
    {"reply":"_notgood","problems":[<problem id1>,<problem id2>]}
    ```
    
    `<problem id#>`: is problem id, has value in 1000-9999

      - 2449: bad data type
      - 4333: unsupported service
      - 6206: already have account
      - 1618: id not exist or wrong password
      - 3237: not login yet
      - 2487: don't have devices or this device is not online
      - 2938: use email difference than the email that was login

